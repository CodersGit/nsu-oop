#include "record_array.h"
#include <stdio.h>
#include <crtdbg.h>

int main(void) {
	RecordArray * ra = ra_create(5, sizeof(int));
	int i = 1;
	printf("Prepared\n");
	ra_set(ra, 2, &i);
	printf("Set passed\n");
	printf("First array element, in first border :%d\n", *((int*)ra_get(ra, 2)));
	ra_set(ra, 6, &i);
	printf("Set passed\n");
	printf("Second array element, NOT in first border :%d\n", *((int*)ra_get(ra, 6)));
	ra_delete(ra);
	system("pause");
	_CrtDumpMemoryLeaks();
	return 0;
}