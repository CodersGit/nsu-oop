#include "record_array.h"

#define SHIFT(ptr,i)((void*)((char*)ptr+i))

// ������� ����� ������ ���������� ������� size, ������ �������� record_size
RecordArray* ra_create(size_t size, size_t record_size) {
	if (size < 0 || record_size <= 0) return NULL;
	RecordArray* ra = (RecordArray*) malloc(sizeof(RecordArray));
	assert(ra);
	ra->size = size;
	ra->record_size = record_size;
	ra->array = (void*)malloc(size * record_size);
	return ra;
}

// ���������� ������
void ra_delete(RecordArray *arr) {
	if (!arr) return;
	if (arr->array) free(arr->array);
	arr->size = 0;
	arr->record_size = 0;
	arr->array = NULL;
	free(arr);
	return;
}

// �������� ��������� �� ������ �� �������
void *ra_get(RecordArray *arr, size_t idx) {
	if (arr->size <= idx || arr->size <= 0 || !arr) return NULL;
	return SHIFT(arr->array, idx*arr->record_size);
}

// �������� ������ � �������, ���������� ������ �� record
void ra_set(RecordArray *arr, size_t idx, void *record) {
	if(idx < 0 || !arr) return;
	if (arr->size < idx) {
		void * new_array = realloc(arr->array, (idx + 1) * arr->record_size);
		if (new_array != NULL) arr->array = new_array;
		else return;
		arr->size = idx + 1;
	}
	memcpy(SHIFT(arr->array, idx*arr->record_size), record, (int) arr->record_size);
}