#include <stdlib.h>
#include <assert.h>
#include <malloc.h>
#include <memory.h>
#ifndef RECORD_ARRAY_H
#define RECORD_ARRAY_H

typedef struct _RecordArray {
	void* array;
	size_t size;
	size_t record_size;
} RecordArray;

// ������� ����� ������ ���������� ������� size, ������ �������� record_size
RecordArray *ra_create(size_t size, size_t record_size);

// ���������� ������
void ra_delete(RecordArray *arr);

// �������� ��������� �� ������ �� �������
void *ra_get(RecordArray *arr, size_t idx);

// �������� ������ � �������, ���������� ������ �� record
void ra_set(RecordArray *arr, size_t idx, void *record);

#endif