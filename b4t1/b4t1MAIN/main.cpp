#include <iostream>
#include <string>
#include <map>
#include "template_parser.h"

using namespace std; 
using namespace cstm;

int main() {
	TemplateParser tp("123 {{! deleted comment}}  some text {{#test}}This will not be removed{{/test}} {{^test2}}And this too{{/test2}} {{test3}}");
	try {
		map <string, string> tplconst = {{"test",""}, {"test2","false"}, {"test3","That's text inserted by templater"}};
		tp.parse(tplconst);
		cout << tp.getResult() << endl;
		for(auto a : tplconst)
			cout << a.first << " " << a.second << endl;
	} catch(TagNotClosedException) {
		cerr << "Cought TagNotClosedException" << endl;
	} catch(CantFindTagPairException) {
		cerr << "Cought CantFindTagPairException" << endl;
	}
	system("pause");
	return 0;
}