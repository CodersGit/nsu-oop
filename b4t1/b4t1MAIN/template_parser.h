#ifndef TPARSER_H_LOADED
#define TPARSER_H_LOADED

#include <string>
#include <map>
#include <algorithm>
#include <iterator>
#include <exception>

namespace cstm {
	class TemplateParserSyntaxException: std::exception				{};
	class TagNotClosedException: TemplateParserSyntaxException		{};
	class CantFindTagPairException: TemplateParserSyntaxException {};
	class VariableNotDefinedException: TemplateParserSyntaxException	{};
	class TemplateParser {
	public:
		TemplateParser(std::string tplget);
		void parse(const std::map<std::string, std::string> &tplconst);
		std::string getResult();
	private:
		std::string btpl; //before
		std::string atpl; //after
	};
}
#endif