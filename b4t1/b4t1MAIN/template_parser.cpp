#include "template_parser.h"

cstm::TemplateParser::TemplateParser(std::string tplget) {
	btpl = tplget;
}

void cstm::TemplateParser::parse(const std::map<std::string, std::string> &tplconst) {
	size_t searchafter = 0, elem;
	copy(btpl.begin(), btpl.end(), back_inserter(atpl));
	while((elem = atpl.find("{{", searchafter)) != std::string::npos) {
		size_t endelem = atpl.find("}}", elem + 2);
		if(endelem != std::string::npos)
			switch(atpl[elem + 2]) {
				case '#': {
					std::string nconst = atpl.substr(elem + 3, endelem - elem - 3);
					size_t endif = atpl.find("{{/" + nconst + "}}", endelem + 2);
					if(endif == std::string::npos)
						throw CantFindTagPairException();
					if(tplconst.find(nconst) == tplconst.end())
						throw VariableNotDefinedException();
					if(tplconst.at(nconst) == "false")
						atpl.erase(elem, endif + 5 + nconst.size() - elem);
					else {
						atpl.erase(endif, 5 + nconst.size());
						atpl.erase(elem, 5 + nconst.size());
					}
				}
						  break;
				case '^': {
					std::string nconst = atpl.substr(elem + 3, endelem - elem - 3);
					size_t endif = atpl.find("{{/" + nconst + "}}", endelem + 2);
					if(endif == std::string::npos)
						throw CantFindTagPairException();
					if(tplconst.find(nconst) == tplconst.end())
						throw VariableNotDefinedException();
					if(tplconst.at(nconst) != "false")
						atpl.erase(elem, endif + 5 + nconst.size() - elem);
					else {
						atpl.erase(endif, 5 + nconst.size());
						atpl.erase(elem, 5 + nconst.size());
					}
				}
						  break;
				case '!':
					atpl.erase(elem, endelem - elem + 2);
					break;
				default:
					std::string nconst = atpl.substr(elem + 2, endelem - elem - 2);
					if(tplconst.find(nconst) == tplconst.end())
						throw VariableNotDefinedException();
					atpl.replace(atpl.begin() + elem, atpl.begin() + endelem + 2, tplconst.at(nconst).c_str());
					break;
			}
		else
			throw TagNotClosedException();
			searchafter = elem;
	}
}

std::string cstm::TemplateParser::getResult() {
	return atpl;
}
