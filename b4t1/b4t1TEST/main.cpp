#include <iostream>
#include "../b4t1MAIN/template_parser.h"
#include <gtest/gtest.h>

using namespace std;
using namespace cstm;

TEST (SuccessWayTest, NewDefault) {
	TemplateParser tp("123 {{! deleted comment}}  some text {{#test}}This will not be removed{{/test}} {{^test2}}And this too{{/test2}} {{test3}}");
	tp.parse({{"test",""}, {"test2","false"}, {"test3","That's text inserted by templater"}});
	cout << tp.getResult() << endl;
}

TEST (ThrowTagNotClosedExceptionTest, NewDefault) {
	TemplateParser tp("123 {{! deleted comment}}  some text {{#test}}This will not be removed{{/test}} {{^test2}}And this too{{/test2}} {{test3");
	ASSERT_THROW(tp.parse({{"test",""}, {"test2","false"}, {"test3","That's text inserted by templater"}}), TagNotClosedException);
	cout << tp.getResult() << endl;
}

TEST (ThrowCantFindTagPairExceptionTest, NewDefault) {
	TemplateParser tp("123 {{! deleted comment}}  some text {{#test}}This will not be removed{{/test}} {{^test2}}And this too {{test3}}");
	ASSERT_THROW(tp.parse({{"test",""}, {"test2","false"}, {"test3","That's text inserted by templater"}}), CantFindTagPairException);
	cout << tp.getResult() << endl;
}

TEST (ThrowVariableNotDefinedExceptionTest, NewDefault) {
	TemplateParser tp("123 {{! deleted comment}}  some text {{#test}}This will not be removed{{/test}} {{^test2}}And this too{{/test2}} {{test3}}");
	ASSERT_THROW(tp.parse({{"test",""}, {"test2","false"}}), VariableNotDefinedException);
	cout << tp.getResult() << endl;
}

int main (int argc, char ** argv) {
	::testing::InitGoogleTest (&argc, argv);
	int errnum = RUN_ALL_TESTS ();
	system ("pause");
	return errnum;
}