#include "richboy.h"

static const Profile _Richboy = {
	"Hey!",
	"I have a supercar",
	"I want a dress. Will somebody buy it for me?",
	"Of course, baby. I'm the richest boy in the world!"
};

const void *Richboy = &_Richboy;