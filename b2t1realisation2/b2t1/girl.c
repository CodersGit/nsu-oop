#include "girl.h"

void girl_say_anyway(void* self) {
	const UserInfo* user = (UserInfo*)self;
	say(user, "I want a dress. Will somebody buy it for me?");
}

static const Profile _Girl = {
	"Hey!",
	"I want a dress. Will somebody buy it for me?",
	0,
	0
};

const void *Girl = &_Girl;