#include "profile.h"

UserInfo* NEW(const Profile* type, char* name){
	UserInfo* user = (UserInfo*) malloc(sizeof(UserInfo));
	if (!user) return NULL;
	user->replics = type;
	user->name = name;
	say(user, user->replics->first);
	say_anyway(user);
	react(user);
	return user;
}

void say_anyway(void* self) {
	if (!self)
		return;
	const UserInfo* user = (UserInfo*)self;
	if (user->replics->sayanyway != 0)
	say(user, user->replics->sayanyway);
}

void react(void* self) {
	if (!self)
		return;
	const UserInfo* user = (UserInfo*)self;
	if (user->replics->reactphrase == 0 || user->replics->reactsearchfor == 0)
		return;
	if (strstr(chat_history, user->replics->reactsearchfor));
	say(user, user->replics->reactphrase);
}

void say(UserInfo* user, char* phrase) {
	if (!user || !phrase)
		return;
	chat_history = realloc(chat_history, (strlen(chat_history) + strlen(phrase))*sizeof(char) + 1);//+1 ������ ��� \0
	chat_history = strcat(chat_history, phrase);
	printf("\t\t%s\r%s:\n", phrase, user->name);
}

void DELETE(UserInfo* self) {
	if (!self) return;
	self->name = 0;
	self->replics = 0;
	free(self);
	return;
}