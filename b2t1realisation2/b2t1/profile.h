#ifndef PROFILE_H_LOADED
#define PROFILE_H_LOADED
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <stdio.h>

typedef struct UserStrings {
	char* first;
	char* sayanyway;
	char* reactsearchfor;
	char* reactphrase;
} Profile;

char* chat_history;

typedef struct UserVars {
	Profile* replics;
	char* name;
} UserInfo;

UserInfo* NEW(Profile* type, char* name);
void say_anyway(void* self);
void react(void* self);
void say(UserInfo* user, char* phrase);
void DELETE(UserInfo* user);
#endif