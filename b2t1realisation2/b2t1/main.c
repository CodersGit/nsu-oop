#include <crtdbg.h>
#include "profile.h"
#include "girl.h"
#include "girl_c.h"
#include "boy.h"
#include "richboy.h"
#include "user.h"

int main(void) {
	chat_history = (char*) malloc(sizeof(char));
	*chat_history = 0;

	FILE* users = fopen("users.txt", "r");
	while (!feof(users))	{
		char nick[16], type[30];
		if (fscanf(users, "%15s %30s", &nick, &type) != 2) {
//			printf("file incorrect!");
			continue;
		}
		if (!strcmp(type, "user")) {
			void* tmp_user = NEW(User, nick);
			DELETE(tmp_user);
		} else if (!strcmp(type, "boy")) {
			void* tmp_user = NEW(Boy, nick);
			DELETE(tmp_user);
		} else if (!strcmp(type, "girl")) {
			void* tmp_user = NEW(Girl, nick);
			DELETE(tmp_user);
		} else if (!strcmp(type, "cowardgirl")) {
			void* tmp_user = NEW(GirlC, nick);
			DELETE(tmp_user);
		} else if (!strcmp(type, "richboy")) {
			void* tmp_user = NEW(Richboy, nick);
			DELETE(tmp_user);
		} else {
			continue;
		}
	}
	fclose(users);
	free(chat_history);
	_CrtDumpMemoryLeaks();
	system("pause");
	return 0;
}