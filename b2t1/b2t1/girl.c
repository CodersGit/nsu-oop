#include "girl.h"

void girl_say_anyway(void* self, char** chat_history) {
	if (!self)
		return;
	const UserInfo* user = (UserInfo*)self;
	say (user, "I want a dress. Will somebody buy it for me?", chat_history);
}

static const Profile _Girl = {
	sizeof (UserInfo),
	user_init,
	girl_say_anyway,
	0,
	user_deinit
};

const void *Girl = &_Girl;