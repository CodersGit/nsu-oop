#include <crtdbg.h>
#include "profile.h"
#include "girl.h"
#include "girl_c.h"
#include "boy.h"
#include "richboy.h"
#include "user.h"

int main (void) {
	char* chat_history = (char*) malloc(sizeof(char));
	*chat_history = 0;

	FILE* users = fopen("users.txt", "r");
	if (!users) {
		printf ("Can't read users file!");
		return 0;
	}

	while (!feof(users))	{
		char nick[16], type[30];

		if (fscanf(users, "%15s %30s", &nick, &type) != 2) {
			continue;
		}
		if (!strcmp(type, "user")) {
			UserInfo* tmp_user = NEW (User, nick, &chat_history);
			if (tmp_user->method->sayAnywayPhrase)
				tmp_user->method->sayAnywayPhrase (tmp_user, &chat_history);
			if (tmp_user->method->checkForReaction)
				tmp_user->method->checkForReaction (tmp_user, &chat_history);
			DELETE(tmp_user);
		} else if (!strcmp(type, "boy")) {
			UserInfo* tmp_user = NEW (Boy, nick, &chat_history);
			if (tmp_user->method->sayAnywayPhrase)
				tmp_user->method->sayAnywayPhrase (tmp_user, &chat_history);
			if (tmp_user->method->checkForReaction)
				tmp_user->method->checkForReaction (tmp_user, &chat_history);
			DELETE(tmp_user);
		} else if (!strcmp(type, "girl")) {
			UserInfo* tmp_user = NEW (Girl, nick, &chat_history);
			if (tmp_user->method->sayAnywayPhrase)
				tmp_user->method->sayAnywayPhrase (tmp_user, &chat_history);
			if (tmp_user->method->checkForReaction)
				tmp_user->method->checkForReaction (tmp_user, &chat_history);
			DELETE(tmp_user);
		} else if (!strcmp (type, "cowardgirl")) {
			UserInfo* tmp_user = NEW (GirlC, nick, &chat_history);
			if (tmp_user->method->sayAnywayPhrase)
				tmp_user->method->sayAnywayPhrase (tmp_user, &chat_history);
			if (tmp_user->method->checkForReaction)
				tmp_user->method->checkForReaction (tmp_user, &chat_history);
			DELETE(tmp_user);
		} else if (!strcmp (type, "richboy")) {
			UserInfo* tmp_user = NEW (Richboy, nick, &chat_history);
			if (tmp_user->method->sayAnywayPhrase)
				tmp_user->method->sayAnywayPhrase (tmp_user, &chat_history);
			if (tmp_user->method->checkForReaction)
				tmp_user->method->checkForReaction (tmp_user, &chat_history);
			DELETE(tmp_user);
		} else {
			continue;
		}
	}
	fclose(users);
	free(chat_history);
	_CrtDumpMemoryLeaks();
	system("pause");
	return 0;
}