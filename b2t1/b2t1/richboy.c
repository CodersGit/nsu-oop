#include "richboy.h"

void richboy_say_anyway (void* self, char** chat_history) {
	if (!self)
		return;
	const UserInfo* user = (UserInfo*)self;
	say (user, "I have a supercar", chat_history);
}

void richboy_react (void* self, char** chat_history) {
	if (!self)
		return;
	const UserInfo* user = (UserInfo*)self;
	if (strstr(*chat_history, "I want a dress. Will somebody buy it for me?"));
	say (user, "Of course, baby. I'm the richest boy in the world!", chat_history);
}

static const Profile _Richboy = {
	sizeof (UserInfo),
	user_init,
	richboy_say_anyway,
	richboy_react,
	user_deinit
};

const void *Richboy = &_Richboy;