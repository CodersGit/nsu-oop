#include "profile.h"

UserInfo* NEW(const Profile* type, ...){
	UserInfo* user = (UserInfo*) malloc(type->size);
	if (!user) return NULL;
	user->method = type;
	va_list args;
	va_start (args, type);
	if (type->constructor)
		type->constructor(user, args);
	va_end (args);
	return user;
}

void say(UserInfo* user, char* phrase, char** chat_history) {
	if (!user || !phrase)
		return;
	*chat_history = realloc(*chat_history, (strlen(*chat_history) + strlen(phrase) + 1)*sizeof(char));//+1 ������ ��� \0
	*chat_history = strcat(*chat_history, phrase);
	printf("\t\t%s\r%s:\n", phrase, user->name);
}

void DELETE(UserInfo* self) {
	if (!self) return;
	if (self && self->method && self->method->destructor)
		self->method->destructor(self);
	self->method = 0;
	free(self);
	return;
}