#ifndef PROFILE_H_LOADED
#define PROFILE_H_LOADED
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

typedef struct UserMethods {
	size_t size;
	void *(*constructor)(void *self, va_list data);
	void *(*sayAnywayPhrase)(void *self);
	int (*checkForReaction)(void *self);
	void *( *destructor)(void *self);
} Profile;

typedef struct UserVars {
	Profile* method;
	char* name;
} UserInfo;

UserInfo* NEW(Profile* type, ...);

void say(UserInfo* user, char** phrase);

void DELETE(UserInfo* user);
#endif