#ifndef RICHBOY_H_LOADED
#define RICHBOY_H_LOADED
#include "profile.h"
#include "user.h"

void richboy_say_anyway (void* self, char** chat_history);
void richboy_react (void* self, char** chat_history);

extern const void *Richboy;

#endif