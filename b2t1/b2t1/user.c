#include "user.h"

void user_init (void* self, va_list data) {
	UserInfo* user = (UserInfo*) self;
	user->name = (char*) va_arg (data, char*);
	say (user, "Hey!", va_arg (data, char*));
}

void user_deinit (void* self) {
	UserInfo* user = (UserInfo*) self;
	user->name = NULL;
}

static const Profile _User = {
	sizeof (UserInfo),
	user_init,
	0,
	0,
	user_deinit
};

const void *User = &_User;